package movies.importer;

import java.util.ArrayList;

/**
 * 
 * @author tomas
 *
 */

public class KaggleImporter extends Processor {
	
	public KaggleImporter(String sourceDirec, String outputDirec) {
		super(sourceDirec, outputDirec, true);
	}
	/**
	 * converts an ArrayList into a string
	 * split the string into separate sections
	 * take in the information that we want/need for a movie object
	 * toString the movie object and add it into the new ArrayList
	 * return the ArrayList when we went through the entire file
	 * @param file
	 * @param processedFile
	 */
	public ArrayList<String> process(ArrayList<String> file){
		ArrayList<String> processedFile = new ArrayList<String>();
		ArrayList<Movie> movies = new ArrayList<Movie>();
		for( String row : file) {
			String[] temp = new String[20];
			temp = row.split("\\t", -1);
			Movie tempMovie = new Movie(temp[20], temp[15], temp[13], "kaggle");
			movies.add(tempMovie);
		}
		for (Movie item : movies) {
			processedFile.add(item.toString());
		}
		return processedFile;
	}
}
