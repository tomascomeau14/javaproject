package movies.importer;

import java.util.ArrayList;

public class Deduper extends Processor{
	
	/**
	 * Looks through an arraylist and transforms them into movie objects
	 * Then looks at all of the movie objests that are in a list and removes duplicate movies with similar run times
	 * Then it puts the remainder of the movies back into a string format in a list
	 * Returns a list of movies in String format
	 * @param sourceDir
	 * @param outputDir
	 * @author tomas
	 */

	
	public Deduper(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}
	
	//uses 4 arraylists to determine what we keep and what we remove
	public ArrayList<String> process(ArrayList<String> list){
		ArrayList<String> purified = new ArrayList<String>();
		ArrayList<Movie> movieList = new ArrayList<Movie>();
		ArrayList<Movie> toRemove = new ArrayList<Movie>();
		ArrayList<Movie> toAdd= new ArrayList<Movie>();
		
		//Using this loop to transform the strings into movie objects to work with
		for(String s : list) {
			String[] temp = s.split("\\t", -1);
			Movie test = new Movie(temp[0],temp[1],temp[2],temp[3]);
			movieList.add(test);
		}
		//Trying to find duplicate movies
		for(Movie s : movieList) {
			//If true then there is some duplicate
			boolean match = movieList.contains(s);
			int first = movieList.indexOf(s);
			//Checks to see for duplicates
			if(match) {
				//looping to find all of the duplicates
				for(Movie ss : movieList) {
					s.checkSource(ss);
					movieList.set(first, s);
					toRemove.add(ss);	
					//Checks to see if that movie is already in if it is then it is not added
					if(!toAdd.contains(s)) {
						toAdd.add(s);
					}

				}
			}
		}
		//empties out the movielist array of the movies to be deleted to save some memory space
		movieList.removeAll(toRemove);
		//System.out.println(movieList);
		//Putting the content of toAdd into purified
		for(Movie s : toAdd) {
			purified.add(s.toString());
		}
		return purified;
	}
}
