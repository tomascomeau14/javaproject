package movies.importer;

import java.util.ArrayList;

/**
 * 
 * @author Jimmy
 *
 */
public class Validator extends Processor{

	//Validator constructor
	public Validator(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
		
	}
	
	//Makes sure that the movie has the correct information
	public ArrayList<String> process(ArrayList<String> movieList){
		ArrayList<String> newArr = new ArrayList<String>();
		
		//splitting the strings by the different fields
		for(String s : movieList) {
			String[] temp = s.split("\t", -1);
			
			//check if the year and runtime is a number by trying to parseInt it
			//if it isnt a number, it will raise an exception and skip the part where we add in the movie in a new arraylist
			try {
				int year = Integer.parseInt(temp[0]);
				int runtime = Integer.parseInt(temp[2]);
				
				//if the title is not empty or is not null, add the movie into a new arraylist
				if(!(temp[1].isEmpty()) || !(temp[1].contentEquals(null))) {
					newArr.add(s);
				}
			}
			catch(Exception e){
				
			}
		}
		return newArr;
	}

}
