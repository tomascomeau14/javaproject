package movies.importer;

public class Movie {

	//Information about a movie
	private String releaseYear;
	private String title;
	private String runtime;
	private String source;
	
	
	/**
	 * 
	 * @param releaseYear
	 * @param title
	 * @param runtime
	 * @param source
	 * @author Jimmy
	 */
	public Movie(String releaseYear, String title, String runtime, String source) {
		this.releaseYear = releaseYear;
		this.title = title;
		this.runtime = runtime;
		this.source = source;
	}
	
	/**
	 * getter methods for every private attributes
	 * @author Jimmy
	 */
	public String getReleaseYear() {
		return releaseYear;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getRuntime() {
		return runtime;
	}
	
	public String getSource() {
		return source;
	}
	
	/**
	 * Overriding the toString method
	 * @author Jimmy
	 */
	public String toString() {
		return (releaseYear + "	" +  title + "	" + runtime + "	" + source);
	}
	
	/**
	 * Overrides the equals method for when a movie is involved
	 * @param an object
	 * @author tomas
	 */
	public boolean equals(Object o) {
		Movie a = (Movie)o;
		int run = Integer.parseInt(a.getRuntime());
		int run2 = Integer.parseInt(this.getRuntime());
		String titl1 = a.getTitle();
		String titl2 = this.getTitle();
		String year1 = a.getReleaseYear();
		String year2 = this.getReleaseYear();
		if(titl1.equalsIgnoreCase(titl2)) {
			if(year1.equalsIgnoreCase(year2)) {
				if(run >= run2) {
					run = run - run2;
					if(run <= 5) {
						return true;
					}
					else {
						return false;
					}
				}
				else {
					run = run2 - run;
					if(run <= 5) {
						return true;
					}
					else {
						return false;
					}
				}
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	
	
	/**
	 * This method should be called on by the original movie, and takes as input the duplicate, 
	 * where the source of the original will change depending on what the duplicate has.
	 * @param duplicate
	 * @author jimmy
	 */
	public void checkSource(Movie duplicate) {
		// if the main one is imdb and the duplicate is kaggle, change the source of the main one to imdb;kaggle
		if(this.source.contentEquals("imdb") && duplicate.source.contentEquals("kaggle")) {
			this.source = "imdb;kaggle";
		}
		// if the main one is kaggle and the duplicate is imdb, change the source of the main one to kaggle;imdb
		else if(this.source.contentEquals("kaggle") && duplicate.source.contentEquals("imdb")) {
			this.source = "kaggle;imdb";
		}
		//if it is kaggle/kaggle, imdb/imdb, kaggle;imdb/kaggle;imdb or imdb;kaggle/imdb;kaggle, change nothing
		
	}
	
	
	
}
