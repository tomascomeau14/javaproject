package movies.importer;

import java.util.ArrayList;

/**
 * 
 * @author tomas
 *
 */
public class Normalizer extends Processor {
	
	public Normalizer(String sourceDirec, String outputDirec) {
		super(sourceDirec, outputDirec, false);
	}
	/**
	 * converts an ArrayList into a string
	 * split the string into separate sections
	 * then looks at sections and changes them for a more normalized format
	 * return them into an arraylist
	 * returns the arraylist
	 * @param movies
	 * @param newArr
	 */
	public ArrayList<String> process(ArrayList<String> movies){
		ArrayList<String> newArr = new ArrayList<String>();
		for(String s : movies) {
			String[] temp = s.split("\\t", -1);
			String title = temp[1];
			String runtime = temp[2];
			title = title.toLowerCase();
			String[] temps = runtime.split(" ", -1);
			runtime = temps[0];
			String nstring = temp[0]+"	"+title+"	"+runtime+"	"+temp[3];
			newArr.add(nstring);
		}
		return newArr;
	}
}
