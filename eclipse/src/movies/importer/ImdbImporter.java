package movies.importer;

import java.util.ArrayList;

/**
 * 
 * @author Jimmy
 *
 */
public class ImdbImporter extends Processor{

	/**
	 * ImdbImporter constructor
	 * @param sourceDir
	 * @param outputDir
	 */
	public ImdbImporter(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, true);
	}
	
	/**
	 * converts an ArrayList into a string
	 * split the string into separate sections
	 * take in the information that we want/need for a movie object
	 * toString the movie object and add it into the new ArrayList
	 * return the ArrayList when we went through the entire file
	 * @param file
	 * @return processedFile
	 */
	public ArrayList<String> process(ArrayList<String> file){
		ArrayList<String> processedFile = new ArrayList<String>();
		ArrayList<Movie> movieList = new ArrayList<Movie>();
		
		//looping through the arraylist and converting each movie into movie objects and adding them in an arraylist of movies
		for( String row : file) {
			String[] temp = new String[22];
			
			temp = row.split("\t", -1);
			//gathering information from specific indexes corresponding to the year, title and runtime
			Movie tempMovie = new Movie(temp[3], temp[1], temp[6], "imdb");
			movieList.add(tempMovie);
		}
		
		//converting the movies into a String and adding it into an ArrayList of Strings
		for (Movie item : movieList) {
			processedFile.add(item.toString());
		}
		
		return processedFile;
		
	}

}
