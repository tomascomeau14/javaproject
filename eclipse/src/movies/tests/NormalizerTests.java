package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.Normalizer;
 /**
  * 
  * @author tomas
  *
  */
class NormalizerTests {
	/**
	 * Testing to see if normalizer has worked at intended
	 * @author tomas
	 */
	@Test
	void processerTest1() {
		Normalizer test = new Normalizer("SourceDirec", "OutputDirec");
		ArrayList<String> testFile = new ArrayList<String>();
		testFile.add("2016	The Masked Saint	111 minutes	kaggle");
		testFile.add("1894	Miss Jerry	45	imdb");
		String want = "2016	the masked saint	111	kaggle";
		String got = test.process(testFile).get(0);
		assertEquals(want, got);
		want = "1894	miss jerry	45	imdb";
		got = test.process(testFile).get(1);
		assertEquals(want, got);
	}
	
	/**
	 * Testing to see if the title was already lower case if it would still work
	 * @author tomas
	 */
	@Test
	void processerTest2() {
		Normalizer test = new Normalizer("SourceDirec", "OutputDirec");
		ArrayList<String> testFile = new ArrayList<String>();
		testFile.add("2016	the masked saint	111 minutes	kaggle");
		testFile.add("1894	miss jerry	45	imdb");
		String want = "2016	the masked saint	111	kaggle";
		String got = test.process(testFile).get(0);
		assertEquals(want, got);
		want = "1894	miss jerry	45	imdb";
		got = test.process(testFile).get(1);
		assertEquals(want, got);
	}
	
	/**
	 * Testing to see if the runtime was already only one word if it would still work as intended
	 * @author tomas
	 */
	@Test
	void processerTest3() {
		Normalizer test = new Normalizer("SourceDirec", "OutputDirec");
		ArrayList<String> testFile = new ArrayList<String>();
		testFile.add("2016	The Masked Saint	111	kaggle");
		testFile.add("1894	Miss Jerry	45	imdb");
		String want = "2016	the masked saint	111	kaggle";
		String got = test.process(testFile).get(0);
		assertEquals(want, got);
		want = "1894	miss jerry	45	imdb";
		got = test.process(testFile).get(1);
		assertEquals(want, got);
	}
}
