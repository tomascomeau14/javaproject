package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.ImdbImporter;
import movies.importer.Validator;

/**
 * 
 * @author Jimmy
 *
 */
class ImbdImporterTests {

	
	
	/**
	 * Tests out the process method of ImbdImporter
	 * added 2 rows into a Arraylist<String> simulating the file, 
	 * comparing the item in the 1st index with the intended result
	 * i.e the toString of a Movie object.
	 * @author Jimmy
	 */
	@Test
	void processTest() {
		ImdbImporter test = new ImdbImporter("source", "destination");
		
		ArrayList<String> testfile = new ArrayList<String>();
		
		
		testfile.add("tt0000009	Miss Jerry	Miss Jerry	1894	1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	Blanche Bayliss, William Courtenay, Chauncey Depew	The adventures of a female reporter in the 1890s.	5.9	154					1	2");
		testfile.add("tt0000009	Miss Jerry	Miss Jerry	1895	1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	Blanche Bayliss, William Courtenay, Chauncey Depew	The adventures of a female reporter in the 1890s.	5.9	154					1	2");

		String intended = "1895	Miss Jerry	45	imdb";
		String reality = test.process(testfile).get(1);
		assertEquals(intended, reality);
		
	}

	
	/**
	 * trying to see if removing the tabs in the blank spaces located after the used index would influence the result.
	 * 
	 * @author Jimmy
	 */
	@Test
	void processTest2() {
		ImdbImporter test = new ImdbImporter("source", "destination");
		
		ArrayList<String> testfile = new ArrayList<String>();
		
		
		testfile.add("tt0000009	Miss Jerry	Miss Jerry	1894	1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	Blanche Bayliss, William Courtenay, Chauncey Depew	The adventures of a female reporter in the 1890s.	5.9	154		1	2");


		String intended = "1894	Miss Jerry	45	imdb";
		String reality = test.process(testfile).get(0);
		assertEquals(intended, reality);
		
	}
	
	/**
	 * checking to see if the answer would remain the same if we removed one of the key columns i.e the release year
	 * (there are still tabs surrounding the missing year)
	 * 
	 * @author Jimmy
	 */
	@Test
	void processTest3() {
		ImdbImporter test = new ImdbImporter("source", "destination");
		
		ArrayList<String> testfile = new ArrayList<String>();
		
		
		testfile.add("tt0000009	Miss Jerry	Miss Jerry		1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	Blanche Bayliss, William Courtenay, Chauncey Depew	The adventures of a female reporter in the 1890s.	5.9	154		1	2");


		String intended = "	Miss Jerry	45	imdb";
		String reality = test.process(testfile).get(0);
		assertEquals(intended, reality);
		
	}
	
	
	/**
	 * checking to see if removing a column and the tab before it would impact the results
	 * considering we used indexes in the process method, it shifted all the answers by +1 column, giving us unwanted results
	 * @author Jimmy
	 */
	@Test
	void processTest4() {
		ImdbImporter test = new ImdbImporter("source", "destination");
		
		ArrayList<String> testfile = new ArrayList<String>();
		
		
		testfile.add("tt0000009	Miss Jerry	1894	1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	Blanche Bayliss, William Courtenay, Chauncey Depew	The adventures of a female reporter in the 1890s.	5.9	154		1	2");

		//items in index 3, 1, 6
		String intended = "1894-10-09	Miss Jerry	USA	imdb";
		String reality = test.process(testfile).get(0);
		assertEquals(intended, reality);
		
	}
	
	/**
	 * added more columns at the end of the file,
	 * the String[] has a limit of 22, anything after 22 items won't be put in the array
	 * as long as it doesn't disrupt the index location of the title, year and runtime, code will work
	 * @author Jimmy
	 */
	@Test
	void processTest5() {
		ImdbImporter test = new ImdbImporter("source", "destination");
		
		ArrayList<String> testfile = new ArrayList<String>();
		
		
		testfile.add("tt0000009	Miss Jerry	Miss Jerry	1894	1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	Blanche Bayliss, William Courtenay, Chauncey Depew	The adventures of a female reporter in the 1890s.	5.9	154		1	2	a a	a a	a a	a a	a a	a a	a a	a a	a a	a ");


		String intended = "1894	Miss Jerry	45	imdb";
		String reality = test.process(testfile).get(0);
		assertEquals(intended, reality);
		
	}
	/**
	 * testing to see what happens when the year is a letter
	 */
	@Test
	void processTest6() {
		ImdbImporter test = new ImdbImporter("source", "destination");
		
		ArrayList<String> testfile = new ArrayList<String>();
		
		
		testfile.add("tt0000009	Miss Jerry	Miss Jerry	1894	1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	Blanche Bayliss, William Courtenay, Chauncey Depew	The adventures of a female reporter in the 1890s.	5.9	154					1	2");
		testfile.add("tt0000009	Miss Jerry	Miss Jerry	abc	1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	Blanche Bayliss, William Courtenay, Chauncey Depew	The adventures of a female reporter in the 1890s.	5.9	154					1	2");

		String intended = "abc	Miss Jerry	45	imdb";
		String reality = test.process(testfile).get(1);
		assertEquals(intended, reality);
		
	}
	
	

}
