package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.KaggleImporter;
/**
 * 
 * @author tomas
 *
 */
class KaggleImporterTests {
	/**
	 * Tests the process method of KaggleImporter
	 * added 2 rows into a Arraylist<String> simulating the file,
	 * comparing the item in the 1st index with the intended result
	 * aka the toString method of a movie object.
	 * @author tomas
	 */
	@Test
	void processTest() {
		KaggleImporter test = new KaggleImporter("SourceD", "OutputD");
		ArrayList<String> testFile = new ArrayList<String>();
		testFile.add("Brett Granstaff	Diahann Carroll	Lara Jean Chorostecki	Roddy Piper	T.J. McGibbon	James Preston Rogers	\"The journey of a professional wrestler who becomes a small town pastor and moonlights as a masked vigilante fighting injustice. While facing a crisis at home and at the church, the pastor must evade the police and somehow reconcile his violent secret identity with his calling to be a pastor.\"	Warren P. Sonoda	Director Not Available	Director Not Available	Action	PG-13 	1/8/2016	111 minutes	Freestyle Releasing	The Masked Saint	Scott Crowell	Brett Granstaff	Writer Not Available	Writer Not Available	2016");
		testFile.add("Brett Granstaff	Diahann Carroll	Lara Jean Chorostecki	Roddy Piper	T.J. McGibbon	James Preston Rogers	\"The journey of a professional wrestler who becomes a small town pastor and moonlights as a masked vigilante fighting injustice. While facing a crisis at home and at the church, the pastor must evade the police and somehow reconcile his violent secret identity with his calling to be a pastor.\"	Warren P. Sonoda	Director Not Available	Director Not Available	Action	PG-13 	1/8/2016	111 minutes	Freestyle Releasing	The Masked Saint	Scott Crowell	Brett Granstaff	Writer Not Available	Writer Not Available	2016");
		String want = "2016	The Masked Saint	111 minutes	kaggle";
		String got = test.process(testFile).get(1);
		assertEquals(want, got);
		got = test.process(testFile).get(0);
		assertEquals(want, got);
	}
	
	/**
	 * trying to see if removing the tabs in the blank spaces located after the used index would influence the result
	 * 
	 * @author tomas
	 */
	@Test
	void processTest2() {
		KaggleImporter test = new KaggleImporter("SourceD", "OutputD");
		ArrayList<String> testFile = new ArrayList<String>();
		testFile.add("Brett Granstaff	Diahann Carroll	Lara Jean Chorostecki	Roddy Piper	T.J. McGibbon	James Preston Rogers	\\\"The journey of a professional wrestler who becomes a small town pastor and moonlights as a masked vigilante fighting injustice. While facing a crisis at home and at the church, the pastor must evade the police and somehow reconcile his violent secret identity with his calling to be a pastor.\\\"	Warren P. Sonoda	Director Not Available	Director Not Available	Action	PG-13 	1/8/2016	111 minutes	Freestyle Releasing	The Masked Saint	Scott Crowell	Brett Granstaff	Writer Not Available	Writer Not Available	2016");
		String want = "2016	The Masked Saint	111 minutes	kaggle";
		String got = test.process(testFile).get(0);
		assertEquals(want, got);
		
	}
	
	/**
	 * checking to see if the answer would remain the same if we removed one of the key columns like the first Director of the movie
	 * (Has realized that by removing the tab all the results afterwards will get shifted down by 1 and create an unwanted result)
	 * (there are still tabs surrounding the missing Director)
	 * @author tomas
	 */
	@Test
	void processTest3() {
		KaggleImporter test = new KaggleImporter("SourceD", "OutputD");
		ArrayList<String> testFile = new ArrayList<String>();
		testFile.add("Brett Granstaff	Diahann Carroll	Lara Jean Chorostecki	Roddy Piper	T.J. McGibbon	James Preston Rogers	\\\"The journey of a professional wrestler who becomes a small town pastor and moonlights as a masked vigilante fighting injustice. While facing a crisis at home and at the church, the pastor must evade the police and somehow reconcile his violent secret identity with his calling to be a pastor.\\\"		Director Not Available	Director Not Available	Action	PG-13 	1/8/2016	111 minutes	Freestyle Releasing	The Masked Saint	Scott Crowell	Brett Granstaff	Writer Not Available	Writer Not Available	2016");
		String want = "2016	The Masked Saint	111 minutes	kaggle";
		String got = test.process(testFile).get(0);
		assertEquals(want, got);
	}
	
	/**
	 * checking to see what happens when more coloumns are added at the end of the line
	 * Result: Adding more columns at the end of the line will not affect result as long as it does not mess up the index number
	 * @author tomas
	 */
	@Test
	void processTest4() {
		KaggleImporter test = new KaggleImporter("SourceD", "OutputD");
		ArrayList<String> testFile = new ArrayList<String>();
		testFile.add("Brett Granstaff	Diahann Carroll	Lara Jean Chorostecki	Roddy Piper	T.J. McGibbon	James Preston Rogers	\\\"The journey of a professional wrestler who becomes a small town pastor and moonlights as a masked vigilante fighting injustice. While facing a crisis at home and at the church, the pastor must evade the police and somehow reconcile his violent secret identity with his calling to be a pastor.\\\"		Director Not Available	Director Not Available	Action	PG-13 	1/8/2016	111 minutes	Freestyle Releasing	The Masked Saint	Scott Crowell	Brett Granstaff	Writer Not Available	Writer Not Available	2016	hi	more	see ya	and 	then	some	more");
		String want = "2016	The Masked Saint	111 minutes	kaggle";
		String got = test.process(testFile).get(0);
		assertEquals(want, got);
	}

}
