package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.Validator;

/**
 * 
 * @author Jimmy
 *
 */
class ValidatorTests {
	
	/**
	 * testing to see if the content inside the new arrayList send out the same as the original arrayList
	 */
	@Test
	void processTest1() {
		Validator test = new Validator("source", "destination");
		ArrayList<String> testfile = new ArrayList<String>();
		
		testfile.add("1894	Miss Jerry	45	imdb");
		testfile.add("1895	Miss Jerry	45	imdb");
		testfile.add("1896	Miss Jerry	45	imdb");
		
		String expected = "1896	Miss Jerry	45	imdb";
		String reality = test.process(testfile).get(2);
		
		
		
		assertEquals(expected,reality);
		
		
		
	}
	
	/**
	 * checking to see if a row is deleted when there is an invalid input
	 */
	@Test
	void processTest2() {
		Validator test = new Validator("source", "destination");
		ArrayList<String> testfile = new ArrayList<String>();
		
		testfile.add("1894	Miss Jerry	45	imdb");
		testfile.add("	Miss Jerry	45	imdb");
		testfile.add("1896	Miss Jerry	45	imdb");
		
		String expected = "1896	Miss Jerry	45	imdb";
		String reality = test.process(testfile).get(1);

		assertEquals(expected,reality);
		
		
		
	}
	/**
	 * testing out to see if it properly discards a row when each of the columns have an invalid value
	 */
	@Test
	void processTest3() {
		Validator test = new Validator("source", "destination");
		ArrayList<String> testfile = new ArrayList<String>();
		
		testfile.add("1894	Miss Jerry		imdb"); // runtime is missing
		testfile.add("	Miss Jerry	45	imdb");// year is missing
		testfile.add("1896		45	imdb"); // title is missing
		testfile.add("1896 Miss Jerry	45	imdb"); // the tab between the year and title is a space
		
		int expected = 0; //should have an array of size 0;
		int reality = test.process(testfile).size();
		
		assertEquals(expected,reality);

		
	}
	
	/**
	 * Trying out kaggle related inputs.
	 */
	@Test
	void processTest4() {
		Validator test = new Validator("source", "destination");
		ArrayList<String> testfile = new ArrayList<String>();
		
		testfile.add("2016	the masked saint	111	kaggle"); 
		testfile.add("1896	Miss Jerry	45	imdb"); // 
		testfile.add("2017	the masked saint	111	kaggle");	
		
		String expected = "2017	the masked saint	111	kaggle";
		String reality = test.process(testfile).get(2);
		
		assertEquals(expected,reality);

		
	}
	
	/**
	 * checking to see if the size of the arraylist shrunk when the movie contained invalid info
	 */
	@Test
	void processTest5() {
		Validator test = new Validator("source", "destination");
		ArrayList<String> testfile = new ArrayList<String>();
		
		testfile.add("1894	Miss Jerry	45	imdb");
		testfile.add("b	Miss Jerry	45	imdb");
		testfile.add("1896	Miss Jerry	a	imdb");
		
		int expected = 1;
		int reality = test.process(testfile).size();
		
		
		
		assertEquals(expected,reality);
		
		
		
	}
	

	
	
	

}
