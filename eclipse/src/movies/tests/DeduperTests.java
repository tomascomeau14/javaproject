package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import movies.importer.Deduper;

import org.junit.jupiter.api.Test;

class DeduperTests {

	/**
	 * Testing the deduper method to see if it works with only 2 occurences
	 * And testing if the constructor works.
	 * @author tomas
	 */
	@Test
	void testDeDuper() {
		Deduper dupe = new Deduper("SourceDirec", "OutputDirec");
		ArrayList<String> testFile = new ArrayList<String>();
		testFile.add("2016	the masked saint	111	kaggle");
		testFile.add("2016	the masked saint	111	kaggle");
		ArrayList<String> testFile2 = new ArrayList<String>();
		testFile2.add("2016	the masked saint	111	kaggle");
		testFile = dupe.process(testFile);
		assertEquals(testFile2,testFile);

	}
	/**
	 * Testing the deduper method to see if it works with 3 occurences with different run times
	 * @author tomas
	 */
	@Test
	void testDeduper2() {
		Deduper dupe = new Deduper("SourceDirec", "OutputDirec");
		ArrayList<String> testFile = new ArrayList<String>();
		testFile.add("2016	the masked saint	111	kaggle");
		testFile.add("2016	the masked saint	113	kaggle");
		testFile.add("2016	the masked saint	118	kaggle");
		testFile.add("2016	the masked saint	113	kaggle");
		testFile.add("2016	the masked saint	112	kaggle");
		testFile.add("2016	the masked saint	110	imdb");
		ArrayList<String> testFile2 = new ArrayList<String>();
		testFile2.add("2016	the masked saint	111	kaggle;imdb");
		testFile2.add("2016	the masked saint	118	kaggle;imdb");
		testFile = dupe.process(testFile);
		assertEquals(testFile2,testFile);
	}
}
