package movies.tests;

import static org.junit.jupiter.api.Assertions.*;
/**
 * @author tomas
 */

import movies.importer.Movie;

import org.junit.jupiter.api.Test;

class MovieTests {
	/**
	 * Testing to see if the constructor works as intended as well as the toString method
	 * @author tomas
	 */
	@Test
	void testConstructer() {
		String year = "1990";
		String title = "Hello it's a me, Mario";
		String runtime = "20";
		String source = "MarioBros";
		Movie mario = new Movie(year,title,runtime,source);
		String want = year+"	"+title+"	"+runtime+"	"+source;
		String got = mario.toString();
		assertEquals(want, got);
	}
	
	/**
	 * Testing all of the get methods
	 * @author tomas
	 */
	@Test
	void testGetters() {
		String year = "1990";
		String title = "Hello it's a me, Mario";
		String runtime = "20";
		String source = "MarioBros";
		Movie mario = new Movie(year,title,runtime,source);
		assertEquals(year, mario.getReleaseYear());
		assertEquals(title, mario.getTitle());
		assertEquals(runtime, mario.getRuntime());
		assertEquals(source, mario.getSource());
	}
	
	/**
	 * Tests the equals method
	 * @author tomas
	 */
	@Test
	void testEquals() {
		String year = "1990";
		String title = "Hello it's a me, Mario";
		String runtime = "20";
		String source = "MarioBros";
		Movie mario = new Movie(year,title,runtime,source);
		Movie mario2 = new Movie(year,title,runtime,source);
		boolean match = mario.equals(mario2);
		assertEquals(true,match);
		runtime = "25";
		Movie mario3 = new Movie(year,title,runtime,source);
		match = mario.equals(mario3);
		assertEquals(true,match);
	}
	
	/**
	 * Tests the checkSource method
	 * @author tomas
	 */
	@Test
	void testCheckSource() {
		String year = "1990";
		String title = "Hello it's a me, Mario";
		String runtime = "20";
		String source = "imdb";
		Movie mario = new Movie(year,title,runtime,source);
		Movie mario2 = new Movie(year,title,runtime,source);
		mario.checkSource(mario2);
		assertEquals("imdb", mario.getSource());
		source = "kaggle";
		Movie mario3 = new Movie(year,title,runtime,source);
		mario.checkSource(mario3);
		assertEquals("imdb;kaggle", mario.getSource());
		mario3.checkSource(mario2);
		assertEquals("kaggle;imdb", mario3.getSource());
	}
}
